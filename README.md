# Backup Test Project
Service is cross-platform solution. You can use it for backuping "TODO" service.
## Installation
Install python 2.7 and add it to system variables "path".
##### Dependensies:
1) Django - web framework
2) requests - library for request to "TODO" service, default service host - http://localhost:9000.

Execute command for installation dependencies:
```
pip install -r requirements.txt
```
## Run
For run service execute command:
```
python manage.py runserver 8000
```
## Use case
##### Backup  accounts
This  API  will  initiate  a  complete  backup  of  all  todo  items  in  the  TodoItemServer.  The  backup  is  asynchronous  and  the  API  will  return  the  the  id  for  the  initiated  backup.  

Request:  POST  /backupsRequest  
body:  N/A  
Response  body:  ```{“backupId”:  <backupId>}```

##### List  backups
This  API  will  list  all  backups  that  have  initiated.  Backup  status  is  one  of  the  following:
* In  progress  
* OK
* Failed 

Request:  GET  /backupsRequest  
body:  N/AResponse  
body:  ```    [  {“backupId”:  “<backup  id>”,“date”:  “<backup  date>”,“status”:  “<backup  status>”},        {...        }    ]```

##### Export  backup
This  API  will  return  the  content  of  a  specified  backup  id  the  CSV  format  specified  below.  
Request:  GET  /exports/{backup  id}
Request  body:  N/A
Response  body:  ```Username;TodoItemId;Subject;DueDate;Done{username};{todoitemid};{subject};{duedate};{done}```