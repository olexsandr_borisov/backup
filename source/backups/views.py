from django.http import JsonResponse
from django.views.generic import View
from source.backups.models import BackupModel, BackupListModel


class BackupsView(View):
    http_method_names = ['get', 'post']

    def post(self, request, *args, **kwargs):
        backup = BackupModel()
        BackupListModel().add_backup(backup)
        return JsonResponse({'backupId': backup.id})

    def get(self, request, *args, **kwargs):
        return JsonResponse(BackupListModel().get(), safe=False)
