import csv
import io
import json
import thread
import urlparse
from datetime import datetime
from time import sleep

import requests

BACKUP_STATUS_SUCCESS = 'OK'
BACKUP_STATUS_IN_PROGRESS = 'In  progress'
BACKUP_STATUS_FAILED = 'Failed'

TODO_SERVER_HOST = 'http://localhost:9000'
TODO_LIST_REQUEST = 'users'

CSV_BACKUP_HEADER = ['Username', 'TodoItemId', 'Subject', 'DueDate', 'Done']


class BackupModel(object):
    _id = None
    _date = None
    _status = None
    _data = None

    def __init__(self, ):
        self._status = BACKUP_STATUS_IN_PROGRESS
        self._date = datetime.now()
        self._id = BackupListModel().index
        thread.start_new_thread(self._make, ())

    def _make(self):
        try:
            response = requests.get(urlparse.urljoin(TODO_SERVER_HOST, TODO_LIST_REQUEST))
            self._data = json.loads(response.text)
            self._status = BACKUP_STATUS_SUCCESS
        except:
            self._status = BACKUP_STATUS_FAILED

    @property
    def id(self):
        return self._id

    @property
    def date(self):
        return self._date.strftime("%Y-%m-%d %H:%M:%S")

    @property
    def status(self):
        return self._status

    def get(self):
        if self.status == BACKUP_STATUS_SUCCESS:
            for item in self._data:
                for todo in item['todos']:
                    yield item['username'], todo['id'], todo['subject'], todo['dueDate'], todo['done']


class BackupListModel(object):
    _backup_list = {}
    _index = 0

    @property
    def index(self):
        self._index += 1
        return self._index

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            setattr(cls, 'instance', super(BackupListModel, cls).__new__(cls))
        return getattr(cls, 'instance')

    def add_backup(self, backup):
        self._backup_list[backup.id] = backup

    def get(self):
        return [dict(backupId=backup.id, status=backup.status, date=backup.date) for backup in self._backup_list.values()]

    def get_csv(self, backup_id):
        io_file = io.BytesIO()
        writer = csv.writer(io_file, delimiter=';', quotechar='\n')
        writer.writerow(CSV_BACKUP_HEADER)
        if backup_id in self._backup_list:
            backup = self._backup_list[backup_id]
            for row in backup.get():
                writer.writerow(row)
        return io_file.getvalue()

