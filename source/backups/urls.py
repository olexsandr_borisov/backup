from django.conf.urls import url
from source.backups import views

urlpatterns = [
    url(r'', views.BackupsView.as_view()),
]
