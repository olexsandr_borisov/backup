from django.http import HttpResponse

from source.backups.models import BackupListModel


def export(request, backup_id):
    return HttpResponse(BackupListModel().get_csv(int(backup_id)))
