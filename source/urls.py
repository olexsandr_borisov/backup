from django.conf.urls import url, include
from source import views

urlpatterns = [
    url(r'^backups', include('source.backups.urls')),
    url(r'^exports/(?P<backup_id>\d+)', views.export),
]
